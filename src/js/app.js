"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import Swiper from 'swiper';
import { Navigation } from 'swiper/modules';
import AirDatepicker from 'air-datepicker'

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')


// Маска для ввода номера телефона
maskInput('input[name="phone"]');
maskInput('[data-time]','__-__');

const initSliders = () => {
    if (document.querySelector('[data-gallery]')) {
        new Swiper('[data-gallery]', {
            modules: [Navigation],
            observer: true,
            loop: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 10,
            speed: 600,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            // Arrows
            navigation: {
                nextEl: '[data-gallery-next]',
                prevEl: '[data-gallery-prev]',
            }
        });
    }
}

window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();

    document.querySelectorAll('[data-date]').forEach(el => {
        new AirDatepicker(el,{
            autoClose: true,
            multipleDatesSeparator: ' - '
        });
    });
});
